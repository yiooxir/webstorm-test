import React from 'react'
import ReactDOM from 'react-dom'
import { MyCmp } from 'components/my-test'

const routes = (
  <MyCmp />
)

ReactDOM.render(routes, document.getElementById('app'))
